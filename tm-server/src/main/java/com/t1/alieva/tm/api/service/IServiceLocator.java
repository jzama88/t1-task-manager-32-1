package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.endpoint.DomainEndpoint;
import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {
    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();
}
