package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
