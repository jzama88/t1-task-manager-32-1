package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.data.*;
import com.t1.alieva.tm.dto.response.data.*;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import org.jetbrains.annotations.NotNull;

public interface IDomainEndpoint {
    @NotNull
    DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) throws AccessDeniedException, AbstractFieldException;

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

}
