package com.t1.alieva.tm.dto.response.task;

import com.t1.alieva.tm.dto.response.task.AbstractTaskResponse;
import com.t1.alieva.tm.model.Task;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {
    public TaskChangeStatusByIdResponse(@Nullable Task task) {
        super(task);
    }
}
