package com.t1.alieva.tm.dto.request.task;

import com.t1.alieva.tm.dto.request.user.AbstractUserRequest;
import com.t1.alieva.tm.enumerated.TaskSort;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskListRequest extends AbstractUserRequest {
    @Nullable
    private TaskSort sort;

    public TaskListRequest(@Nullable final TaskSort sort) {
        this.sort = sort;
    }
}
