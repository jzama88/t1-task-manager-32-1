package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.user.AbstractUserResponse;
import com.t1.alieva.tm.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(@Nullable User user) {
        super(user);
    }
}
