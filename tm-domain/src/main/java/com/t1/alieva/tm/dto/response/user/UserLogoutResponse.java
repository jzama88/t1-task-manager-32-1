package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.user.AbstractUserResponse;

public final class UserLogoutResponse extends AbstractUserResponse {
}
