package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.user.*;
import com.t1.alieva.tm.dto.response.user.*;
import org.jetbrains.annotations.NotNull;

public interface IUserEndpoint {

    @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

    @NotNull UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

}
