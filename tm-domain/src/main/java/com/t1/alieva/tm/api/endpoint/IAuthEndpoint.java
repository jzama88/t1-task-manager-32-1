package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.request.user.UserProfileRequest;
import com.t1.alieva.tm.dto.response.user.UserLoginResponse;
import com.t1.alieva.tm.dto.response.user.UserLogoutResponse;
import com.t1.alieva.tm.dto.response.user.UserProfileResponse;
import org.jetbrains.annotations.NotNull;

public interface IAuthEndpoint {

    @NotNull UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull UserProfileResponse profile(@NotNull UserProfileRequest request);

}
