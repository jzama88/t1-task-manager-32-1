package com.t1.alieva.tm.dto.response.user;

import com.t1.alieva.tm.dto.response.user.AbstractUserResponse;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.Nullable;

public final class UserRegistryResponse extends AbstractUserResponse {
    public UserRegistryResponse(@Nullable User user) {
        super(user);
    }
}
