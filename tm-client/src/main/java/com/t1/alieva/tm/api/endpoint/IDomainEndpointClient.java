package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.data.*;
import com.t1.alieva.tm.dto.response.data.*;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface IDomainEndpointClient {
    @NotNull
    DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request) throws IOException, ClassNotFoundException, AbstractException;

    @NotNull
    DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request) throws IOException, ClassNotFoundException, AbstractException;

    @NotNull
    DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) throws IOException, ClassNotFoundException, AbstractException;

    @NotNull
    DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) throws AccessDeniedException, AbstractFieldException;

    @NotNull
    DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) throws IOException, ClassNotFoundException, AbstractException;

    @NotNull
    DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxBRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxBRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) throws IOException, ClassNotFoundException;
}
