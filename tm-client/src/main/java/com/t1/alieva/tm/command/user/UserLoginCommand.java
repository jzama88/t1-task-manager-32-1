package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "User login.";

    @NotNull
    public static final String NAME = "login";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        getAuthEndpoint().login(request);
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}
