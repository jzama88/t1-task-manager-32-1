package com.t1.alieva.tm.component;

import com.t1.alieva.tm.dto.request.data.DataBackupLoadRequest;
import com.t1.alieva.tm.dto.request.data.DataBackupSaveRequest;
import com.t1.alieva.tm.exception.AbstractException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    public static final String FILE_BACKUP = "./task-manager-client/backup.base64";

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }


    public void start() throws AbstractException {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    @SneakyThrows
    public void save() {
        if (bootstrap.getAuthEndpoint().getSocket() != null) {
            bootstrap.getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest());
        }
    }

    public void load() throws AbstractException {
        if (bootstrap.getAuthEndpoint().getSocket() != null) {
            if (!Files.exists(Paths.get(FILE_BACKUP))) return;
            bootstrap.getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest());
        }
    }
}
