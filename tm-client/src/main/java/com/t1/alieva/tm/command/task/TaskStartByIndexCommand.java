package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.project.TaskChangeStatusByIndexRequest;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-start-by-index";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start Project by Index.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(index, Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusByIndex(request);
    }
}
