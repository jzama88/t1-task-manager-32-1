package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.service.ILoggerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Nullable
    private static final String ARGUMENT = null;

    @NotNull
    private static final String DESCRIPTION = "Close application.";

    @NotNull
    private static final String NAME = "exit";

    @Override
    public void execute() {
        @NotNull final ILoggerService loggerService = getServiceLocator().getLoggerService();
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

    @Override
    public @Nullable String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }
}
