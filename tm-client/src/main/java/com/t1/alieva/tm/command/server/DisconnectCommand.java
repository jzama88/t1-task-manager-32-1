package com.t1.alieva.tm.command.server;

import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DisconnectCommand extends AbstractCommand {
    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().disconnect();

    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return "Disconnect from server";
    }

    @Override
    public @NotNull String getName() {
        return "disconnect";
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
