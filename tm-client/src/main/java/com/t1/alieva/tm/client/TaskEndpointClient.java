package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.ITaskEndpointClient;
import com.t1.alieva.tm.dto.request.project.TaskChangeStatusByIndexRequest;
import com.t1.alieva.tm.dto.request.task.*;
import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.request.user.UserProfileRequest;
import com.t1.alieva.tm.dto.response.task.*;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;


@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpoint implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull AbstractEndpoint client) {
        super(client);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request){
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request) {
        return call(request, TaskGetByIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest request) {
        return call(request, TaskGetByIndexResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskListByProjectIdResponse getTaskByProjectId(@NotNull TaskListByProjectIdRequest request)  {
        return call(request, TaskListByProjectIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request){
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("ADMIN", "ADMIN")));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        @NotNull final TaskEndpointClient taskEndpointClient = new TaskEndpointClient(authEndpointClient);
        System.out.println(taskEndpointClient.createTask(new TaskCreateRequest("Fourth Task", "Fourth Task description")));
        // System.out.println(taskEndpointClient.listTask(new TaskListRequest()).getTasks());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}
