package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskChangeStatusByIdRequest;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-start-by-id";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start Project by ID.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(id, Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusById(request);
    }
}
