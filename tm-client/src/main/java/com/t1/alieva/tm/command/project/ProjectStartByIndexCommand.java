package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-start-by-index";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Start Project by Index.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(index, Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }
}
