package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.user.*;
import com.t1.alieva.tm.dto.response.user.*;
import com.t1.alieva.tm.exception.entity.ClassNotFoundException;
import org.jetbrains.annotations.NotNull;
import com.t1.alieva.tm.exception.system.IOException;


public interface IUserEndpointClient {

    @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request) throws IOException, ClassNotFoundException;

    @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) throws IOException, ClassNotFoundException;

    @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) throws IOException, ClassNotFoundException;

    @NotNull UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request) throws IOException, ClassNotFoundException;

    @NotNull UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) throws IOException, ClassNotFoundException;

    @NotNull UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) throws IOException, ClassNotFoundException;
}
