package com.t1.alieva.tm.command.data;

import com.t1.alieva.tm.dto.request.data.DataBackupLoadRequest;
import com.t1.alieva.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load backup from file";

    @NotNull
    public static final String NAME = "backup-load";

    @SneakyThrows
    @Override
    public void execute() {
        serviceLocator.getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest());
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
