package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.IAuthEndpointClient;
import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.request.user.UserProfileRequest;
import com.t1.alieva.tm.dto.response.user.UserLoginResponse;
import com.t1.alieva.tm.dto.response.user.UserLogoutResponse;
import com.t1.alieva.tm.dto.response.user.UserProfileResponse;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;


@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpoint implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpoint client) {
        super(client);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request){
        return call(request, UserLoginResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request){
        return call(request, UserLogoutResponse.class);
    }


    @Override
    @NotNull
    @SneakyThrows
    public UserProfileResponse profile(@NotNull final UserProfileRequest request){
        return call(request, UserProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("ADMIN", "ADMIN")));
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}
