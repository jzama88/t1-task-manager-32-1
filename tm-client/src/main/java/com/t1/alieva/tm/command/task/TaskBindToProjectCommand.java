package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.task.TaskBindToProjectRequest;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.AbstractSystemException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-bind-to-project";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Task bind to project.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request;
        request = new TaskBindToProjectRequest(projectId, taskId);
        getTaskEndpoint().bindTaskToProject(request);
    }
}
