package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Logout current user.";

    @NotNull
    public static final String NAME = "logout";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        getAuthEndpoint().logout(request);
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }
}
