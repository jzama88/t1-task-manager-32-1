package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.IDomainEndpointClient;
import com.t1.alieva.tm.dto.request.data.*;
import com.t1.alieva.tm.dto.request.user.UserLoginRequest;
import com.t1.alieva.tm.dto.request.user.UserLogoutRequest;
import com.t1.alieva.tm.dto.response.data.*;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;


@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpoint implements IDomainEndpointClient {


    public DomainEndpointClient(@NotNull AbstractEndpoint client) {
        super(client);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @Override
    @NotNull
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull DataJsonLoadJaxBRequest request) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull DataJsonSaveJaxBRequest request) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request){
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull DataXmlLoadJaxBRequest request) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request){
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull DataXmlSaveJaxBRequest request){
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request){
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @SneakyThrows
    @Override
    @NotNull
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request){
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataBase64SaveRequest());
        }
        {
            System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
            @NotNull final DomainEndpointClient domainClient = new DomainEndpointClient(authEndpointClient);
            domainClient.saveDataBase64(new DataBase64SaveRequest());
        }
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}
