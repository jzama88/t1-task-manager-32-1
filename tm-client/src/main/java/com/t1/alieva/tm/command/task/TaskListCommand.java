package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.dto.request.project.ProjectListRequest;
import com.t1.alieva.tm.dto.request.task.TaskListRequest;
import com.t1.alieva.tm.enumerated.ProjectSort;
import com.t1.alieva.tm.enumerated.TaskSort;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "t-list-show";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Display Task list.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(sort);
        @Nullable final List<Task> tasks = getTaskEndpoint().listTask(request).getTasks();
        renderTasks(tasks);
    }
}
