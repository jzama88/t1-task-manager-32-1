package com.t1.alieva.tm.api.endpoint;

import com.t1.alieva.tm.dto.request.project.TaskChangeStatusByIndexRequest;
import com.t1.alieva.tm.dto.request.task.*;
import com.t1.alieva.tm.dto.response.task.*;
import com.t1.alieva.tm.exception.entity.ClassNotFoundException;
import org.jetbrains.annotations.NotNull;
import com.t1.alieva.tm.exception.system.IOException;



public interface ITaskEndpointClient {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskGetByIdResponse getTaskById(@NotNull TaskGetByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskGetByIndexResponse getTaskByIndex(@NotNull TaskGetByIndexRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskListByProjectIdResponse getTaskByProjectId(@NotNull TaskListByProjectIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) throws IOException, ClassNotFoundException;

    @NotNull
    TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) throws IOException, ClassNotFoundException;
}
