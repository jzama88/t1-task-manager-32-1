package com.t1.alieva.tm.client;

import com.t1.alieva.tm.api.endpoint.IUserEndpointClient;
import com.t1.alieva.tm.dto.request.user.*;
import com.t1.alieva.tm.dto.response.user.*;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;


@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpoint implements IUserEndpointClient {

    public UserEndpointClient(@NotNull final AbstractEndpoint client) {
        super(client);
    }

    @Override
    @SneakyThrows
    public @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request)  {
        return call(request, UserUnlockResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request)  {
        return call(request, UserUpdateProfileResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }
}
