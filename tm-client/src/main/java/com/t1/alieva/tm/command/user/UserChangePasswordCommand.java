package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.dto.request.user.UserChangePasswordRequest;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;


public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @NotNull
    public static final String NAME = "user-change-password";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(password);
        getUserEndpoint().changeUserPassword(request);
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }
}
