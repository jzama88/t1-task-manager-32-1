package com.t1.alieva.tm.client;

import com.t1.alieva.tm.dto.response.ApplicationErrorResponse;
import com.t1.alieva.tm.exception.AbstractException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 864;

    @Nullable
    protected Socket socket;

    public AbstractEndpoint(
            @NotNull final AbstractEndpoint client

    ) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }


    @NotNull
    @SneakyThrows
    protected <T> T call(@Nullable final Object data, @NotNull final Class<T> clazz)  {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @SneakyThrows
    private ObjectInputStream getObjectInputStream() {
        return new ObjectInputStream(getInputStream());
    }

    @Nullable
    private OutputStream getOutputStream() throws IOException {
        if (socket == null) return null;
        return socket.getOutputStream();
    }

    @Nullable
    @SneakyThrows
    private InputStream getInputStream(){
        if (socket == null) return null;
        return socket.getInputStream();
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    @Nullable
    public void disconnect() throws IOException {
        if (socket == null) return;
        socket.close();
    }
}